import json
import requests
import re
import urllib3
from collections import Counter

urllib3.disable_warnings()

response = requests.get("https://en.wikipedia.org/w/api.php?action=query&prop=extracts&pageids=21721040&explaintext&format=json")
match_words = json.loads(response.text.lower())

text = match_words['query']['pages']['21721040']['extract']

match_pattern = re.findall(r"\b\w{4,15}\b", text)

counter = Counter(match_pattern)
group = {}

for key in counter:
    if not counter[key] in group:
        group[counter[key]] = []
    group[counter[key]].append(key)

for key in sorted(group, reverse=True)[0:5]:
    print (str(key) + " " + ", ".join(str(p) for p in group[key]))